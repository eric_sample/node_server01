console.log("▶▷▶▷ [item.js]");
var express = require('express');
var router = express.Router();
var Item = require('../model/Products');
var mongoose = require('mongoose');

//get all items
router.get('/', function(req, res, next) {
    Item.find(function(err,items) {
        if(err) return next(err);
        res.json(items);
    });
});

//single item
router.get('/:id', function(req,res,next) {
    Item.find({"item_name":req.params.id}, function(err,post) {
        if(err) return next(err);
        res.json(post);
    });
});

//get single item (POST)
router.post('/get', function(req,res,next) {
    console.log(req.body)

    if(req.body.item_name == null || req.body.item_name == '') {
        Item.find(function(err,items) {
            if(err) return next(err);
            res.json(items);
        });
    } else {
        Item.find({"item_name":req.body.item_name}, function(err,post) {
            if(err) return next(err);
            res.json(post);
        });
    }
});


//save item
router.post('/', function(req,res,next) {
    if(req.body.item_name == null || req.body.item_name == '') {
        res.send("입력값이 존재하지 않습니다. (저장실패)");
    } else {
        Item.create(req.body, function(err, post) {
            console.log(req.body);
            if(err) return next(err);
            res.send("저장하였습니다.");
        });
    }
});

//update
router.put('/:id', function(req,res,next) {
    if(!mongoose.Types.ObjectId.isValid(req.params.id)) {
        res.send("조건값(ObjectId : "+req.params.id+")이 유효하지 않습니다.");
    } else {
        Item.exists({_id:req.params.id}).then(exists => {
            if(exists) {
                if(req.body.item_name == null || req.body.item_name == '') {
                    res.send("입력값이 존재하지 않습니다. (업데이트실패)");
                } else {
                    Item.findByIdAndUpdate(req.params.id, req.body, function(err,post) {
                        if(err) {
                            return next(err);
                        } else {
                            res.send("저장하였습니다.");
                        }
                    });
                }
            } else {
                res.send("조건값(ObjectId : "+req.params.id+")이 존재하지 않습니다.");
            }
        })
    }
});
 
//delete
router.delete('/:id', function(req, res, next) {
    if(!mongoose.Types.ObjectId.isValid(req.params.id)) {
        res.send("조건값(ObjectId : "+req.params.id+")이 유효하지 않습니다.");
    } else {
        Item.exists({_id:req.params.id}).then(exists => {
            if(exists) {
                Item.findByIdAndRemove(req.params.id, res.body, function(err,post) {
                    if(err) {
                        return next(err);
                    } else {
                        res.send("삭제하였습니다.");
                    }
                });
            } else {
                res.send("조건값(ObjectId : "+req.params.id+")이 존재하지 않습니다.");
            }
        })
    }
});

module.exports = router;
