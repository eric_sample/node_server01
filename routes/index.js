console.log("▶▷▶▷ [index.js]");
const express = require('express');
const router =express.Router();

const main = require('./main');
const dashboard = require('./dashboard.js');
const about = require('./about.js');
const items = require('./items.js');

const tranInsert = require('./tranInsert.js');
const tranSearch = require('./tranSearch.js');
const tranDelete = require('./tranDelete.js');
const tranUpdate = require('./tranUpdate.js');

router.use('/', main);
router.use('/dashboard', dashboard);
router.use('/about', about);
router.use('/tranInsert', tranInsert);
router.use('/tranSearch', tranSearch);
router.use('/tranDelete', tranDelete);
router.use('/tranUpdate', tranUpdate);
router.use('/items', items);

module.exports = router;