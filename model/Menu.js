var createMenu = function() {
    console.log("▶▷▶▷ create table menu");
    var arr_menu = [{id:'about', name:"About", url:"/about"},
                    {id:"tranInsert", name:"Transaction(입력)", url:"/tranInsert"},
                    {id:"tranSearch", name:"Transaction(조회)", url:"/tranSearch"},
                    {id:"tranDelete", name:"Transaction(삭제)", url:"/tranDelete"},
                    {id:"tranUpdate", name:"Transaction(수정)", url:"/tranUpdate"}];
    var str_menu = '';
    arr_menu.forEach(function(element) {
        str_menu += '<a class="dropdown-item" href="'+element.url+'">'+element.name+'</a>';
    });
    return str_menu;
}
